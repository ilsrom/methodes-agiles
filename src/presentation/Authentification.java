package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import metier.DAO;
import metier.SingletonConnection;
import metier.Utilisateur;

import java.awt.SystemColor;

import javax.swing.JPasswordField;

public class Authentification extends JFrame {

	private JPanel contentPane;
	private JTextField usernamefield;
	private DAO metier;
	private JPasswordField passwordfield;

	/**
	 * Lancer l'application.
	 **/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Authentification frame = new Authentification();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Cr�er la fen�tre.
	 */
	public Authentification() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("COVID19");
		setResizable(false);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		metier = SingletonConnection.getMetier();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(60, 179, 113));
		panel.setBounds(0, 0, 694, 72);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("GESTION DE VACCINATIONS COVID19");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(132, 22, 429, 28);
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(160, 160, 160), 2, true));
		panel_1.setBounds(171, 127, 351, 244);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		usernamefield = new JTextField();
		usernamefield.setBounds(176, 83, 134, 29);
		panel_1.add(usernamefield);
		usernamefield.setColumns(10);
		
		JLabel lblPseudo = new JLabel("Nom d'utilisateur :");
		lblPseudo.setBounds(40, 87, 110, 17);
		panel_1.add(lblPseudo);
		lblPseudo.setForeground(UIManager.getColor("SplitPaneDivider.draggingColor"));
		lblPseudo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel lblMotDePasse = new JLabel("Mot de passe :");
		lblMotDePasse.setBounds(40, 129, 89, 17);
		panel_1.add(lblMotDePasse);
		lblMotDePasse.setForeground(UIManager.getColor("ScrollBar.trackHighlightForeground"));
		lblMotDePasse.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel lblAuthentification = new JLabel("AUTHENTIFICATION");
		lblAuthentification.setBounds(40, 23, 270, 29);
		panel_1.add(lblAuthentification);
		lblAuthentification.setForeground(SystemColor.controlDkShadow);
		lblAuthentification.setHorizontalAlignment(SwingConstants.CENTER);
		lblAuthentification.setFont(new Font("Tahoma", Font.BOLD, 22));
		
		passwordfield = new JPasswordField();
		passwordfield.setBounds(176, 125, 134, 29);
		panel_1.add(passwordfield);
		
		JButton btnNewButton = new JButton("Se connecter");
		btnNewButton.setBackground(new Color(60, 179, 113));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(192, 177, 118, 36);
		panel_1.add(btnNewButton);
		getRootPane().setDefaultButton(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = usernamefield.getText().toString();
				String password = passwordfield.getText().toString();
				
				if(username.equals("") || password.equals("")){
					JOptionPane.showMessageDialog(null, "Remplissez les champs vides s'il vous pla�t.");
				}else{
					Utilisateur utilisateur = metier.authentifier(username, password);
					if(utilisateur.getId() == 0){
						JOptionPane.showMessageDialog(null, "Nom d'utilisateur ou mot de passe incorrect.");
					}else{
						if(utilisateur.getRole().equals("infirmier")){
							EspaceInfirmier espaceInfirmier = new EspaceInfirmier(utilisateur.getId());
							dispose();
							espaceInfirmier.setVisible(true);
						}else if (utilisateur.getRole().equals("responsable")) {
							EspaceResponsable espaceResponsable = new EspaceResponsable();
							dispose();
							espaceResponsable.setVisible(true);
						}
					}
				}
			}
		});
	}
}

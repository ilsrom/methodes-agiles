package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import metier.DAO;
import metier.SingletonConnection;

public class GestionCommandes extends JFrame {

	private JPanel contentPane;
	private DAO metier;
	private JTextField rechercherfield;
	private JTextField referencefield;
	private JTextField nominfirmierfield;
	private JTextField etatfield;
	private JTextField quantitefield;
	private JTable table;
	private int idCommande = 0;

	/**
	 * Launch the application.
	 **/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionCommandes frame = new GestionCommandes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GestionCommandes() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("COVID19");
		setResizable(false);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		metier = SingletonConnection.getMetier();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(60, 179, 113));
		panel.setBounds(0, 0, 694, 72);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ESPACE RESPONSABLE - GESTION DES COMMANDES");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(55, 22, 584, 28);
		panel.add(lblNewLabel);
		
		//---------------------------------------------------------------------------
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(160, 160, 160), 2, true));
		panel_1.setBounds(22, 99, 348, 309);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblPaneauDeGestion = new JLabel("PANEAU DE GESTION");
		lblPaneauDeGestion.setBounds(56, 11, 236, 27);
		lblPaneauDeGestion.setForeground(SystemColor.controlDkShadow);
		lblPaneauDeGestion.setHorizontalAlignment(SwingConstants.CENTER);
		lblPaneauDeGestion.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_1.add(lblPaneauDeGestion);
		
		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(referencefield.getText().toString().equals("") || nominfirmierfield.getText().toString().equals("") || quantitefield.getText().toString().equals("") || etatfield.getText().toString().equals("")){
					JOptionPane.showMessageDialog(null, "Selectionner une commande en attente s'il vous pla�t.");
				}else{
					metier.annulerCommande(idCommande);
					metier.listCommandes(table);
					configurerTable();
					JOptionPane.showMessageDialog(null, "Commande annul�e avec succ�s.");
					referencefield.setText("");
					nominfirmierfield.setText("");
					etatfield.setText("");
					quantitefield.setText("");
					idCommande = 0;
				}
				
			}
		});
		btnAnnuler.setForeground(Color.WHITE);
		btnAnnuler.setBackground(new Color(112, 128, 144));
		btnAnnuler.setBounds(242, 267, 83, 31);
		btnAnnuler.setFocusPainted(false);
		panel_1.add(btnAnnuler);
		
		JButton btnModifier = new JButton("Modifier");
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(referencefield.getText().toString().equals("") || nominfirmierfield.getText().toString().equals("") || quantitefield.getText().toString().equals("") || etatfield.getText().toString().equals("")){
					JOptionPane.showMessageDialog(null, "Selectionner une commande en attente et remplissez les champs vides s'il vous pla�t.");
				}else{
					int vaccin = metier.vaccinId(referencefield.getText().toString());
					int utilisateur = metier.utilisateurId(nominfirmierfield.getText().toString());
					int quantite = Integer.parseInt(quantitefield.getText().toString());
					if(vaccin == 0 || utilisateur == 0){
						JOptionPane.showMessageDialog(null, "Le vaccin ou l'infirmier mentionn�s n'existent pas !");
					}else{
						metier.modifierCommande(idCommande, vaccin, utilisateur, quantite);
						metier.listCommandes(table);
						configurerTable();
						JOptionPane.showMessageDialog(null, "Informations modifi�es avec succ�s.");
						referencefield.setText("");
						nominfirmierfield.setText("");
						etatfield.setText("");
						quantitefield.setText("");
						idCommande = 0;
					}
				}
				
			}
		});
		btnModifier.setForeground(Color.WHITE);
		btnModifier.setBackground(new Color(112, 128, 144));
		btnModifier.setBounds(143, 267, 89, 31);
		btnModifier.setFocusPainted(false);
		panel_1.add(btnModifier);
		
		referencefield = new JTextField();
		referencefield.setColumns(10);
		referencefield.setBounds(151, 65, 174, 27);
		panel_1.add(referencefield);
		
		nominfirmierfield = new JTextField();
		nominfirmierfield.setColumns(10);
		nominfirmierfield.setBounds(151, 103, 174, 27);
		panel_1.add(nominfirmierfield);
		
		etatfield = new JTextField();
		etatfield.setEditable(false);
		etatfield.setColumns(10);
		etatfield.setBounds(151, 179, 174, 27);
		panel_1.add(etatfield);
		
		JLabel prenomlabel = new JLabel("Nom infirmier :");
		prenomlabel.setForeground(SystemColor.controlDkShadow);
		prenomlabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		prenomlabel.setBounds(22, 109, 97, 14);
		panel_1.add(prenomlabel);
		
		JLabel villelabel = new JLabel("Quantit\u00E9 :");
		villelabel.setForeground(SystemColor.controlDkShadow);
		villelabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		villelabel.setBounds(22, 146, 132, 14);
		panel_1.add(villelabel);
		
		JLabel nomLabel = new JLabel("R\u00E9f\u00E9rence vaccin :");
		nomLabel.setForeground(SystemColor.controlDkShadow);
		nomLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		nomLabel.setBounds(22, 71, 110, 14);
		panel_1.add(nomLabel);
		
		JLabel vehiculelabel = new JLabel("Etat :");
		vehiculelabel.setBounds(22, 184, 132, 14);
		panel_1.add(vehiculelabel);
		vehiculelabel.setForeground(SystemColor.controlDkShadow);
		vehiculelabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		quantitefield = new JTextField();
		quantitefield.setBounds(151, 141, 174, 27);
		panel_1.add(quantitefield);
		quantitefield.setColumns(10);
		
		JButton btnCommander = new JButton("Commander");
		btnCommander.setBounds(22, 267, 111, 31);
		panel_1.add(btnCommander);
		btnCommander.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(referencefield.getText().toString().equals("") || nominfirmierfield.getText().toString().equals("") || quantitefield.getText().toString().equals("")){
					JOptionPane.showMessageDialog(null, "Remplissez les champs vides s'il vous pla�t.");
				}else{
					int vaccin = metier.vaccinId(referencefield.getText().toString());
					int utilisateur = metier.utilisateurId(nominfirmierfield.getText().toString());
					int quantite = Integer.parseInt(quantitefield.getText().toString());
					int etat = metier.etatCommande(vaccin, utilisateur);
					int quantiteStock = metier.quantiteVaccin(vaccin);
					int quantiteRestee = quantiteStock - quantite;
					
					if((metier.commandeExiste(vaccin, utilisateur) == true) && (etat == 0)){
						JOptionPane.showMessageDialog(null, "La commande existe d�j� et en attende !");
					}else{
						if(vaccin == 0 || utilisateur == 0){
							JOptionPane.showMessageDialog(null, "Le vaccin ou l'infirmier mentionn�s n'existent pas !");
						}else{
							if(quantiteRestee < 0){
								JOptionPane.showMessageDialog(null, "La quantit� demand�e n'est pas diponible au stock !");
							}else{
								metier.livrerVaccin(vaccin, quantiteRestee);
								metier.ajouterCommande(vaccin, utilisateur, quantite, 1);
								JOptionPane.showMessageDialog(null, "Commande ajout�e et sold�e avec succ�s.");
								if(quantiteRestee < 50){
									JOptionPane.showMessageDialog(null, "ALERTE ! Le stock de ce vaccin est insuffisant, une commande d'approvisionnement automatique va �tre faite.");
									metier.approvisionnementAutomatique(vaccin, (metier.quantiteVaccin(vaccin)+50));
								}
								metier.listCommandes(table);
								configurerTable();
								referencefield.setText("");
								nominfirmierfield.setText("");
								etatfield.setText("");
								quantitefield.setText("");
								idCommande = 0;
							}
						}
					}	
				}
			}
		});
		btnCommander.setForeground(Color.WHITE);
		btnCommander.setBackground(new Color(112, 128, 144));
		btnCommander.setFocusPainted(false);
		
		JButton btnConfirmerLaCommander = new JButton("Confirmer la commande");
		btnConfirmerLaCommander.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(referencefield.getText().toString().equals("") || nominfirmierfield.getText().toString().equals("") || quantitefield.getText().toString().equals("") || etatfield.getText().toString().equals("")){
					JOptionPane.showMessageDialog(null, "Selectionner une commande en attente s'il vous pla�t.");
				}else{
					int vaccin = metier.vaccinId(referencefield.getText().toString());
					int quantiteCommande = Integer.parseInt(quantitefield.getText().toString());
					int quantiteStock = metier.quantiteVaccin(vaccin);
					int quantiteRestee = quantiteStock - quantiteCommande;
					if(quantiteRestee < 0){
						JOptionPane.showMessageDialog(null, "La quantit� demand�e n'est pas diponible au stock !");
					}else{
						metier.livrerVaccin(vaccin, quantiteRestee);
						metier.solderCommande(idCommande);
						JOptionPane.showMessageDialog(null, "Commande confirm�e et livr�e avec succ�s.");
						if(quantiteRestee < 50){
							JOptionPane.showMessageDialog(null, "ALERTE ! Le stock de ce vaccin est insuffisant, une commande d'approvisionnement automatique va �tre faite.");
							metier.approvisionnementAutomatique(vaccin, (metier.quantiteVaccin(vaccin)+50));
						}
						metier.listCommandes(table);
						configurerTable();
						referencefield.setText("");
						nominfirmierfield.setText("");
						etatfield.setText("");
						quantitefield.setText("");
						idCommande = 0;
					}
					
				}
				
			}
		});
		btnConfirmerLaCommander.setForeground(Color.WHITE);
		btnConfirmerLaCommander.setFocusPainted(false);
		btnConfirmerLaCommander.setBackground(new Color(112, 128, 144));
		btnConfirmerLaCommander.setBounds(22, 225, 210, 31);
		panel_1.add(btnConfirmerLaCommander);
		
		JButton btnEffacer = new JButton("Effacer");
		btnEffacer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				referencefield.setText("");
				nominfirmierfield.setText("");
				etatfield.setText("");
				quantitefield.setText("");
				btnConfirmerLaCommander.setEnabled(true);
				btnAnnuler.setEnabled(true);
				btnModifier.setEnabled(true);
				idCommande = 0;
			}
		});
		btnEffacer.setForeground(Color.WHITE);
		btnEffacer.setFocusPainted(false);
		btnEffacer.setBackground(new Color(112, 128, 144));
		btnEffacer.setBounds(242, 225, 83, 31);
		panel_1.add(btnEffacer);
		
		rechercherfield = new JTextField();
		rechercherfield.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String rechercheCle = String.valueOf(rechercherfield.getText().toString());
				metier.rechercherCommandes(table, rechercheCle);
				configurerTable();
			}
		});
		rechercherfield.setBounds(530, 99, 134, 31);
		contentPane.add(rechercherfield);
		rechercherfield.setColumns(10);
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EspaceResponsable espaceResponsable = new EspaceResponsable();
				dispose();
				espaceResponsable.setVisible(true);
			}
		});
		btnRetour.setForeground(Color.WHITE);
		btnRetour.setBackground(new Color(165, 42, 42));
		btnRetour.setBounds(575, 419, 89, 31);
		btnRetour.setFocusPainted(false);
		contentPane.add(btnRetour);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(380, 141, 284, 265);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				int ligneClique = table.getSelectedRow();
				idCommande =  Integer.parseInt((String.valueOf(table.getValueAt(ligneClique, 0))));
				referencefield.setText(metier.referenceVaccin(Integer.parseInt((String.valueOf(table.getValueAt(ligneClique, 1))))));
				nominfirmierfield.setText(metier.nomInfirmier(Integer.parseInt((String.valueOf(table.getValueAt(ligneClique, 2))))));
				if(Integer.parseInt((String.valueOf(table.getValueAt(ligneClique, 4)))) == 0){
					etatfield.setText("En attente");
					btnConfirmerLaCommander.setEnabled(true);
					btnAnnuler.setEnabled(true);
					btnModifier.setEnabled(true);
				}else{
					etatfield.setText("Livr�e");
					btnConfirmerLaCommander.setEnabled(false);
					btnAnnuler.setEnabled(false);
					btnModifier.setEnabled(false);
				}
				quantitefield.setText(String.valueOf(table.getValueAt(ligneClique, 3)));
				
			}
		});
		scrollPane.setViewportView(table);
		metier.listCommandes(table);
		
		JLabel lblNom = new JLabel("Num\u00E9ro commande :");
		lblNom.setForeground(SystemColor.controlDkShadow);
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNom.setBounds(380, 107, 140, 14);
		contentPane.add(lblNom);
		configurerTable();
	}
	
	public void configurerTable(){
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(0).setMaxWidth(50);
		table.getColumnModel().getColumn(2).setMinWidth(90);
		table.getColumnModel().getColumn(2).setMaxWidth(90);
		table.getColumnModel().getColumn(4).setMinWidth(0);
		table.getColumnModel().getColumn(4).setMaxWidth(0);
	}
}

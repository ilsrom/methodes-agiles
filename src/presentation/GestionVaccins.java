package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.TableModel;

import metier.DAO;
import metier.SingletonConnection;
import metier.Utilisateur;
import net.proteanit.sql.DbUtils;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;

import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.JScrollPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GestionVaccins extends JFrame {

	private JPanel contentPane;
	private DAO metier;
	private JTextField rechercherfield;
	private JTextField referencefield;
	private JTextField laboratoirefield;
	private JTextField fabriquantfield;
	private JTextField quantitefield;
	private JTextField depotfield;
	private JTable table;
	private int idVaccin = 0;

	/**
	 * Launch the application.
	 **/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionVaccins frame = new GestionVaccins();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GestionVaccins() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("COVID19");
		setResizable(false);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		metier = SingletonConnection.getMetier();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(60, 179, 113));
		panel.setBounds(0, 0, 694, 72);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ESPACE RESPONSABLE - GESTION DES VACCINS");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(78, 22, 537, 28);
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(160, 160, 160), 2, true));
		panel_1.setBounds(22, 99, 348, 309);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblPaneauDeGestion = new JLabel("PANEAU DE GESTION");
		lblPaneauDeGestion.setBounds(56, 11, 236, 27);
		lblPaneauDeGestion.setForeground(SystemColor.controlDkShadow);
		lblPaneauDeGestion.setHorizontalAlignment(SwingConstants.CENTER);
		lblPaneauDeGestion.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_1.add(lblPaneauDeGestion);
		
		JButton btnSupprimer = new JButton("Supprimer");
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String reference = referencefield.getText().toString();
				String laboratoire = laboratoirefield.getText().toString();
				String fabriquant = fabriquantfield.getText().toString();
				
				if(reference.equals("") || laboratoire.equals("") || fabriquant.equals("") || quantitefield.getText().toString().equals("") || depotfield.getText().toString().equals("")){
					JOptionPane.showMessageDialog(null, "Selectionner un vaccin s'il vous pla�t.");
				}else{
					metier.retirerVaccin(idVaccin);
					metier.listVaccins(table);
					configurerTable();
					JOptionPane.showMessageDialog(null, "Vaccin retit� avec succ�s.");
					referencefield.setText("");
					laboratoirefield.setText("");
					fabriquantfield.setText("");
					quantitefield.setText("");
					depotfield.setText("");
					idVaccin = 0;
				}
				
			}
		});
		btnSupprimer.setForeground(Color.WHITE);
		btnSupprimer.setBackground(new Color(112, 128, 144));
		btnSupprimer.setBounds(228, 267, 97, 31);
		btnSupprimer.setFocusPainted(false);
		panel_1.add(btnSupprimer);
		
		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String reference = referencefield.getText().toString();
				String laboratoire = laboratoirefield.getText().toString();
				String fabriquant = fabriquantfield.getText().toString();
				
				if(reference.equals("") || laboratoire.equals("") || fabriquant.equals("") || quantitefield.getText().toString().equals("") || depotfield.getText().toString().equals("")){
					JOptionPane.showMessageDialog(null, "Remplissez les champs vides s'il vous pla�t.");
				}else{
					int quantite = Integer.parseInt(quantitefield.getText().toString());
					int depot = Integer.parseInt(depotfield.getText().toString());
					if(metier.vaccinExiste(reference)){
						JOptionPane.showMessageDialog(null, "Le vaccin existe d�j� !");
					}else{
						metier.ajouterVaccin(reference, laboratoire, fabriquant, quantite, depot);
						metier.listVaccins(table);
						configurerTable();
						JOptionPane.showMessageDialog(null, "Vaccin ajout� au stock avec succ�s.");
						referencefield.setText("");
						laboratoirefield.setText("");
						fabriquantfield.setText("");
						quantitefield.setText("");
						depotfield.setText("");
						idVaccin = 0;
					}
				}
				
			}
		});
		btnAjouter.setForeground(Color.WHITE);
		btnAjouter.setBackground(new Color(112, 128, 144));
		btnAjouter.setBounds(22, 267, 89, 31);
		btnAjouter.setFocusPainted(false);
		panel_1.add(btnAjouter);
		
		JButton btnModifier = new JButton("Modifier");
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String reference = referencefield.getText().toString();
				String laboratoire = laboratoirefield.getText().toString();
				String fabriquant = fabriquantfield.getText().toString();
				
				if(reference.equals("") || laboratoire.equals("") || fabriquant.equals("") || quantitefield.getText().toString().equals("") || depotfield.getText().toString().equals("")){
					JOptionPane.showMessageDialog(null, "Selectionner un vaccin et remplissez les champs vides s'il vous pla�t.");
				}else{
					int quantite = Integer.parseInt(quantitefield.getText().toString());
					int depot = Integer.parseInt(depotfield.getText().toString());
					metier.modifierVaccin(idVaccin, reference, laboratoire, fabriquant, quantite, depot);
					metier.listVaccins(table);
					configurerTable();
					JOptionPane.showMessageDialog(null, "Informations modifi� avec succ�s.");
					referencefield.setText("");
					laboratoirefield.setText("");
					fabriquantfield.setText("");
					quantitefield.setText("");
					depotfield.setText("");
					idVaccin = 0;
				}
				
			}
		});
		btnModifier.setForeground(Color.WHITE);
		btnModifier.setBackground(new Color(112, 128, 144));
		btnModifier.setBounds(121, 267, 97, 31);
		btnModifier.setFocusPainted(false);
		panel_1.add(btnModifier);
		
		referencefield = new JTextField();
		referencefield.setColumns(10);
		referencefield.setBounds(129, 63, 196, 27);
		panel_1.add(referencefield);
		
		laboratoirefield = new JTextField();
		laboratoirefield.setColumns(10);
		laboratoirefield.setBounds(129, 96, 196, 27);
		panel_1.add(laboratoirefield);
		
		fabriquantfield = new JTextField();
		fabriquantfield.setColumns(10);
		fabriquantfield.setBounds(129, 129, 196, 27);
		panel_1.add(fabriquantfield);
		
		quantitefield = new JTextField();
		quantitefield.setColumns(10);
		quantitefield.setBounds(129, 162, 196, 27);
		panel_1.add(quantitefield);
		
		depotfield = new JTextField();
		depotfield.setColumns(10);
		depotfield.setBounds(129, 194, 196, 27);
		panel_1.add(depotfield);
		
		JLabel lblLaboratoire = new JLabel("Laboratoire :");
		lblLaboratoire.setForeground(SystemColor.controlDkShadow);
		lblLaboratoire.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblLaboratoire.setBounds(22, 101, 89, 14);
		panel_1.add(lblLaboratoire);
		
		JLabel lblDateDeFabrication = new JLabel("Fabriquant :");
		lblDateDeFabrication.setForeground(SystemColor.controlDkShadow);
		lblDateDeFabrication.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDateDeFabrication.setBounds(22, 134, 132, 14);
		panel_1.add(lblDateDeFabrication);
		
		JLabel lblQuantite = new JLabel("Quantit\u00E9 :");
		lblQuantite.setForeground(SystemColor.controlDkShadow);
		lblQuantite.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblQuantite.setBounds(22, 167, 89, 14);
		panel_1.add(lblQuantite);
		
		JLabel lblDpt = new JLabel("D\u00E9p\u00F4t :");
		lblDpt.setForeground(SystemColor.controlDkShadow);
		lblDpt.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDpt.setBounds(22, 199, 89, 14);
		panel_1.add(lblDpt);
		
		JLabel label_4 = new JLabel("R\u00E9f\u00E9rence :");
		label_4.setForeground(SystemColor.controlDkShadow);
		label_4.setFont(new Font("Tahoma", Font.BOLD, 12));
		label_4.setBounds(22, 68, 89, 14);
		panel_1.add(label_4);
		
		JButton btnEffacerLesChamps = new JButton("Effacer les champs");
		btnEffacerLesChamps.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				referencefield.setText("");
				laboratoirefield.setText("");
				fabriquantfield.setText("");
				quantitefield.setText("");
				depotfield.setText("");
				idVaccin = 0;
			}
		});
		btnEffacerLesChamps.setForeground(Color.WHITE);
		btnEffacerLesChamps.setFocusPainted(false);
		btnEffacerLesChamps.setBackground(new Color(112, 128, 144));
		btnEffacerLesChamps.setBounds(22, 230, 303, 31);
		panel_1.add(btnEffacerLesChamps);
		
		rechercherfield = new JTextField();
		rechercherfield.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				
				String rechercheCle = rechercherfield.getText().toString();
				metier.rechercherVaccins(table, rechercheCle);
				configurerTable();
			}
		});
		rechercherfield.setBounds(479, 99, 185, 31);
		contentPane.add(rechercherfield);
		rechercherfield.setColumns(10);
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EspaceResponsable espaceResponsable = new EspaceResponsable();
				dispose();
				espaceResponsable.setVisible(true);
			}
		});
		btnRetour.setForeground(Color.WHITE);
		btnRetour.setBackground(new Color(165, 42, 42));
		btnRetour.setBounds(575, 419, 89, 31);
		btnRetour.setFocusPainted(false);
		contentPane.add(btnRetour);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(380, 141, 284, 265);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				int ligneClique = table.getSelectedRow();
				idVaccin =  Integer.parseInt((String.valueOf(table.getValueAt(ligneClique, 0))));
				referencefield.setText((String) table.getValueAt(ligneClique, 1));
				laboratoirefield.setText((String) table.getValueAt(ligneClique, 2));
				fabriquantfield.setText((String) table.getValueAt(ligneClique, 3));
				quantitefield.setText(String.valueOf(table.getValueAt(ligneClique, 4)));
				depotfield.setText(String.valueOf(table.getValueAt(ligneClique, 5)));
				
			}
		});
		scrollPane.setViewportView(table);
		metier.listVaccins(table);
		
		JLabel label = new JLabel("R\u00E9f\u00E9rence :");
		label.setForeground(SystemColor.controlDkShadow);
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(380, 107, 89, 14);
		contentPane.add(label);
		configurerTable();
		
	}
	
	public void configurerTable(){
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(3).setMinWidth(0);
		table.getColumnModel().getColumn(3).setMaxWidth(0);
		table.getColumnModel().getColumn(4).setMinWidth(0);
		table.getColumnModel().getColumn(4).setMaxWidth(50);
		table.getColumnModel().getColumn(5).setMinWidth(0);
		table.getColumnModel().getColumn(5).setMaxWidth(50);
	}
}

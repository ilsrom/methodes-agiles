package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import metier.DAO;
import metier.SingletonConnection;

public class GestionFournisseurs extends JFrame {

	private JPanel contentPane;
	private DAO metier;
	private JTextField rechercherfield;
	private JTextField nomfield;
	private JTextField prenomfield;
	private JTextField villefield;
	private JTextField vehiculefield;
	private JTextField telephonefield;
	private JTable table;
	private int idFournisseur = 0;

	/**
	 * Launch the application.
	**/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionFournisseurs frame = new GestionFournisseurs();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GestionFournisseurs() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("COVID19");
		setResizable(false);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		metier = SingletonConnection.getMetier();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(60, 179, 113));
		panel.setBounds(0, 0, 694, 72);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ESPACE RESPONSABLE - GESTION DES FOURNISSEURS");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(40, 22, 613, 28);
		panel.add(lblNewLabel);
		
		//----------------------------------------------------------------------------
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(160, 160, 160), 2, true));
		panel_1.setBounds(22, 99, 348, 309);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblPaneauDeGestion = new JLabel("PANEAU DE GESTION");
		lblPaneauDeGestion.setBounds(56, 11, 236, 27);
		lblPaneauDeGestion.setForeground(SystemColor.controlDkShadow);
		lblPaneauDeGestion.setHorizontalAlignment(SwingConstants.CENTER);
		lblPaneauDeGestion.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_1.add(lblPaneauDeGestion);
		
		JButton btnSupprimer = new JButton("Supprimer");
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String nom = nomfield.getText().toString();
				String prenom = prenomfield.getText().toString();
				String ville = villefield.getText().toString();
				String vehicule = vehiculefield.getText().toString();
				String telephone = telephonefield.getText().toString();
				
				if(nom.equals("") || prenom.equals("") || ville.equals("") || vehicule.equals("") || telephone.equals("")){
					JOptionPane.showMessageDialog(null, "Selectionner un fournisseur s'il vous pla�t.");
				}else{
					metier.retirerFournisseur(idFournisseur);
					metier.listFournisseurs(table);
					configurerTable();
					JOptionPane.showMessageDialog(null, "Fournisseur retit� avec succ�s.");
					nomfield.setText("");
					prenomfield.setText("");
					villefield.setText("");
					vehiculefield.setText("");
					telephonefield.setText("");
					idFournisseur = 0;
				}
				
			}
		});
		btnSupprimer.setForeground(Color.WHITE);
		btnSupprimer.setBackground(new Color(112, 128, 144));
		btnSupprimer.setBounds(228, 267, 97, 31);
		btnSupprimer.setFocusPainted(false);
		panel_1.add(btnSupprimer);
		
		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String nom = nomfield.getText().toString();
				String prenom = prenomfield.getText().toString();
				String ville = villefield.getText().toString();
				String vehicule = vehiculefield.getText().toString();
				String telephone = telephonefield.getText().toString();
				
				if(nom.equals("") || prenom.equals("") || ville.equals("") || vehicule.equals("") || telephone.equals("")){
					JOptionPane.showMessageDialog(null, "Remplissez les champs vides s'il vous pla�t.");
				}else{
					if(metier.fournisseurExiste(nom)){
						JOptionPane.showMessageDialog(null, "Le fournisseur existe d�j� !");
					}else{
						metier.ajouterFournisseur(nom, prenom, ville, vehicule, telephone);
						metier.listFournisseurs(table);
						configurerTable();
						JOptionPane.showMessageDialog(null, "Fournisseur ajout� avec succ�s.");
						nomfield.setText("");
						prenomfield.setText("");
						villefield.setText("");
						vehiculefield.setText("");
						telephonefield.setText("");
						idFournisseur = 0;
					}
				}
				
			}
		});
		btnAjouter.setForeground(Color.WHITE);
		btnAjouter.setBackground(new Color(112, 128, 144));
		btnAjouter.setBounds(22, 267, 89, 31);
		btnAjouter.setFocusPainted(false);
		panel_1.add(btnAjouter);
		
		JButton btnModifier = new JButton("Modifier");
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String nom = nomfield.getText().toString();
				String prenom = prenomfield.getText().toString();
				String ville = villefield.getText().toString();
				String vehicule = vehiculefield.getText().toString();
				String telephone = telephonefield.getText().toString();
				
				if(nom.equals("") || prenom.equals("") || ville.equals("") || vehicule.equals("") || telephone.equals("")){
					JOptionPane.showMessageDialog(null, "Selectionner un fournisseur et remplissez les champs vides s'il vous pla�t.");
				}else{
					metier.modifierFournisseur(idFournisseur, nom, prenom, ville, vehicule, telephone);
					metier.listFournisseurs(table);
					configurerTable();
					JOptionPane.showMessageDialog(null, "Informations modifi�es avec succ�s.");
					nomfield.setText("");
					prenomfield.setText("");
					villefield.setText("");
					vehiculefield.setText("");
					telephonefield.setText("");
					idFournisseur = 0;
				}
				
			}
		});
		btnModifier.setForeground(Color.WHITE);
		btnModifier.setBackground(new Color(112, 128, 144));
		btnModifier.setBounds(121, 267, 97, 31);
		btnModifier.setFocusPainted(false);
		panel_1.add(btnModifier);
		
		nomfield = new JTextField();
		nomfield.setColumns(10);
		nomfield.setBounds(129, 63, 196, 27);
		panel_1.add(nomfield);
		
		prenomfield = new JTextField();
		prenomfield.setColumns(10);
		prenomfield.setBounds(129, 95, 196, 27);
		panel_1.add(prenomfield);
		
		villefield = new JTextField();
		villefield.setColumns(10);
		villefield.setBounds(129, 127, 196, 27);
		panel_1.add(villefield);
		
		vehiculefield = new JTextField();
		vehiculefield.setColumns(10);
		vehiculefield.setBounds(129, 159, 196, 27);
		panel_1.add(vehiculefield);
		
		telephonefield = new JTextField();
		telephonefield.setColumns(10);
		telephonefield.setBounds(129, 191, 196, 27);
		panel_1.add(telephonefield);
		
		JLabel prenomlabel = new JLabel("Pr\u00E9nom :");
		prenomlabel.setForeground(SystemColor.controlDkShadow);
		prenomlabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		prenomlabel.setBounds(22, 100, 89, 14);
		panel_1.add(prenomlabel);
		
		JLabel villelabel = new JLabel("Ville :");
		villelabel.setForeground(SystemColor.controlDkShadow);
		villelabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		villelabel.setBounds(22, 132, 132, 14);
		panel_1.add(villelabel);
		
		JLabel vehiculelabel = new JLabel("V\u00E9hicule :");
		vehiculelabel.setForeground(SystemColor.controlDkShadow);
		vehiculelabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		vehiculelabel.setBounds(22, 164, 89, 14);
		panel_1.add(vehiculelabel);
		
		JLabel telephonelabel = new JLabel("T\u00E9l\u00E9phone :");
		telephonelabel.setForeground(SystemColor.controlDkShadow);
		telephonelabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		telephonelabel.setBounds(22, 196, 89, 14);
		panel_1.add(telephonelabel);
		
		JLabel nomLabel = new JLabel("Nom :");
		nomLabel.setForeground(SystemColor.controlDkShadow);
		nomLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		nomLabel.setBounds(22, 68, 89, 14);
		panel_1.add(nomLabel);
		
		JButton btnEffecerLesChamps = new JButton("Effacer les champs");
		btnEffecerLesChamps.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nomfield.setText("");
				prenomfield.setText("");
				villefield.setText("");
				vehiculefield.setText("");
				telephonefield.setText("");
				idFournisseur = 0;
			}
		});
		btnEffecerLesChamps.setForeground(Color.WHITE);
		btnEffecerLesChamps.setFocusPainted(false);
		btnEffecerLesChamps.setBackground(new Color(112, 128, 144));
		btnEffecerLesChamps.setBounds(22, 227, 303, 34);
		panel_1.add(btnEffecerLesChamps);
		
		rechercherfield = new JTextField();
		rechercherfield.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				
				String rechercheCle = rechercherfield.getText().toString();
				metier.rechercherFournisseurs(table, rechercheCle);
				configurerTable();
			}
		});
		rechercherfield.setBounds(530, 99, 134, 31);
		contentPane.add(rechercherfield);
		rechercherfield.setColumns(10);
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EspaceResponsable espaceResponsable = new EspaceResponsable();
				dispose();
				espaceResponsable.setVisible(true);
			}
		});
		btnRetour.setForeground(Color.WHITE);
		btnRetour.setBackground(new Color(165, 42, 42));
		btnRetour.setBounds(575, 419, 89, 31);
		btnRetour.setFocusPainted(false);
		contentPane.add(btnRetour);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(380, 141, 284, 265);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				int ligneClique = table.getSelectedRow();
				idFournisseur =  Integer.parseInt((String.valueOf(table.getValueAt(ligneClique, 0))));
				nomfield.setText((String) table.getValueAt(ligneClique, 1));
				prenomfield.setText((String) table.getValueAt(ligneClique, 2));
				villefield.setText((String) table.getValueAt(ligneClique, 3));
				vehiculefield.setText(String.valueOf(table.getValueAt(ligneClique, 4)));
				telephonefield.setText(String.valueOf(table.getValueAt(ligneClique, 5)));
				
			}
		});
		scrollPane.setViewportView(table);
		metier.listFournisseurs(table);
		
		JLabel lblNom = new JLabel("Nom de fournisseur :");
		lblNom.setForeground(SystemColor.controlDkShadow);
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNom.setBounds(380, 107, 140, 14);
		contentPane.add(lblNom);
		configurerTable();
	}
	
	public void configurerTable(){
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(4).setMinWidth(0);
		table.getColumnModel().getColumn(4).setMaxWidth(0);
		table.getColumnModel().getColumn(5).setMinWidth(80);
		table.getColumnModel().getColumn(5).setMaxWidth(150);
	}

}

package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import metier.DAO;
import metier.SingletonConnection;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EspaceResponsable extends JFrame {

	private JPanel contentPane;
	private DAO metier;

	/**
	 * Launch the application.
	 **/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EspaceResponsable frame = new EspaceResponsable();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EspaceResponsable() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("COVID19");
		setResizable(false);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		metier = SingletonConnection.getMetier();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(60, 179, 113));
		panel.setBounds(0, 0, 694, 72);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ESPACE RESPONSABLE - ACCUEIL");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(159, 22, 375, 28);
		panel.add(lblNewLabel);
		
		JButton btnGestionDesVaccins = new JButton("Gestion des vaccins");
		btnGestionDesVaccins.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GestionVaccins gestionVaccins = new GestionVaccins();
				dispose();
				gestionVaccins.setVisible(true);
			}
		});
		btnGestionDesVaccins.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnGestionDesVaccins.setBackground(new Color(60, 179, 113));
		btnGestionDesVaccins.setForeground(Color.WHITE);
		btnGestionDesVaccins.setBounds(233, 129, 228, 59);
		contentPane.add(btnGestionDesVaccins);
		btnGestionDesVaccins.setFocusPainted(false);
		
		JButton btnGestionDesFournisseurs = new JButton("Gestion des fournisseurs");
		btnGestionDesFournisseurs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GestionFournisseurs gestionFournisseurs = new GestionFournisseurs();
				dispose();
				gestionFournisseurs.setVisible(true);
			}
		});
		btnGestionDesFournisseurs.setForeground(Color.WHITE);
		btnGestionDesFournisseurs.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnGestionDesFournisseurs.setBackground(new Color(60, 179, 113));
		btnGestionDesFournisseurs.setBounds(233, 199, 228, 59);
		contentPane.add(btnGestionDesFournisseurs);
		btnGestionDesFournisseurs.setFocusPainted(false);
		
		JButton btnGestionDesCommandes = new JButton("Se d\u00E9connecter");
		btnGestionDesCommandes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Authentification authentification = new Authentification();
				dispose();
				authentification.setVisible(true);
			}
		});
		btnGestionDesCommandes.setForeground(Color.WHITE);
		btnGestionDesCommandes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnGestionDesCommandes.setBackground(new Color(60, 179, 113));
		btnGestionDesCommandes.setBounds(233, 339, 228, 59);
		contentPane.add(btnGestionDesCommandes);
		btnGestionDesCommandes.setFocusPainted(false);
		
		JButton button = new JButton("Gestion des commandes");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GestionCommandes gestionCommandes = new GestionCommandes();
				dispose();
				gestionCommandes.setVisible(true);
			}
		});
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Tahoma", Font.PLAIN, 14));
		button.setFocusPainted(false);
		button.setBackground(new Color(60, 179, 113));
		button.setBounds(233, 269, 228, 59);
		contentPane.add(button);
	}
}

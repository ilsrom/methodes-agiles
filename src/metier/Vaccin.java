package metier;

/**
 * Classe repr�sente un vaccin. 
 * @author Walid MORSLI
 *
 */

public class Vaccin {

	private int id;
	private String reference;
	private String laboratoire;
	private String fabriquant;
	private int quantite;
	private int depot;
	
	/**
	 * Construire un vaccin en sp�cifiant les param�tres.
	 * @param reference le nom du vaccin
	 * @param laboratoire le laboratoire qui a fabriqu� le vaccin
	 * @param fabriquant le pays qui a fabriquer le vaccin
	 * @param quantite la quantit� du vaccin au stock
	 * @param depot	le d�p�t ou est stock� le vaccin
	 */
	public Vaccin(String reference, String laboratoire, String fabriquant,
			int quantite, int depot) {
		super();
		this.reference = reference;
		this.laboratoire = laboratoire;
		this.fabriquant = fabriquant;
		this.quantite = quantite;
		this.depot = depot;
	}
	/**
	 * Construire un vaccin sans param�tres.
	 */
	public Vaccin() {
		super();
	}
	/**
	 * Retourner l'identifiant du vaccin.
	 * @return entier repr�sente l'identifiant
	 */
	public int getId() {
		return id;
	}
	/**
	 * Remplacer l'id du vaccin avec un autre.
	 * @param id un entier repr�sente le nouveau identifiant
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Retourner la r�f�rence du vaccin.
	 * @return chaine de caract�res repr�sente la r�f�rence
	 */
	public String getReference() {
		return reference;
	}
	/**
	 * Remplacer la r�f�rence du vaccin avec une autre.
	 * @param reference une chaine de caract�res repr�sente la nouvelle r�f�rence
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}
	/**
	 * Retourner le nom du laboratoire.
	 * @return chaine de caract�res repr�sente le nom du laboratoire
	 */
	public String getLaboratoire() {
		return laboratoire;
	}	
	/**
	 * Remplacer le nom du laboratoire avec un autre.
	 * @param laboratoire une chaine de caract�res repr�sente le nouveau laboratoire
	 */
	public void setLaboratoire(String laboratoire) {
		this.laboratoire = laboratoire;
	}
	/**
	 * Retourner le pays du vaccin.
	 * @return chaine de caract�res repr�sente le pays
	 */
	public String getFabriquant() {
		return fabriquant;
	}
	/**
	 * Remplacer le pays du vaccin avec un autre.
	 * @param fabriquant une chaine de caract�res repr�sente le nouveau pays
	 */
	public void setFabriquant(String fabriquant) {
		this.fabriquant = fabriquant;
	}
	/**
	 * Retourner la quantit� du vaccin.
	 * @return un entier repr�sente la quantit�
	 */
	public int getQuantite() {
		return quantite;
	}
	/**
	 * Remplacer la quantit� du vaccin avec une autre.
	 * @param quantite un entier repr�sente la nouvelle quantit�
	 */
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	/**
	 * Retourner le d�p�t du vaccin.
	 * @return un entier repr�sente le d�p�t
	 */
	public int getDepot() {
		return depot;
	}
	/**
	 * Remplacer le d�p�t du vaccin avec un autre.
	 * @param depot un entier repr�sente la nouveau d�p�t
	 */
	public void setDepot(int depot) {
		this.depot = depot;
	}
}

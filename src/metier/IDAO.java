package metier;

import javax.swing.JTable;

/**
 * Interface qui repr�sente la couche d'acc�s aux donn�es
 * @author Walid MORSLI
 *
 */

public interface IDAO {
	
	// Gestion de l'authentification
	/**
	 * V�rifier si le pseudo et le mot de passe renseig�s sont corr�ctes et existent dans la BDD.
	 * @param pseudo le pseudo de l'utilisateur 
	 * @param password le mot de passe de l'utilisateur
	 * @return un objet Utilisateur
	 */
	public Utilisateur authentifier(String pseudo, String password);
	/**
	 * V�rifier si le nom de l'infirmier renseig� existe dans la BDD.
	 * @param nom le nom de l'utilisateur
	 * @return un bool�en, si c'est true il existe sinon il n'existe pas
	 */
	public boolean infirmierExiste(String nom);
	/**
	 * Retourner l'identifiant de l'utilisateur.
	 * @param nom le nom de l'utilisatur
	 * @return un entier repr�sente l'identifiant
	 */
	public int utilisateurId(String nom);
	/**
	 * Retourner le nom de l'infirmier.
	 * @param id l'identifiant de l'infirmier
	 * @return une chaine de caract�res repr�sente le nom
	 */
	public String nomInfirmier(int id);
	//-------------------------------------------------------------------------------------------------------------
	// Gestion des vaccins
	/**
	 * Ajouter un nouveau vaccin.
	 * @param reference le nom du vaccin
	 * @param laboratoire le laboratoire qui a fabriqu� le vaccin
	 * @param fabriquant le pays qui a fabriquer le vaccin
	 * @param quantite la quantit� du vaccin au stock
	 * @param depot	le d�p�t ou est stock� le vaccin
	 */
	public void ajouterVaccin(String reference, String laboratoire, String fabriquant, int quantite, int depot);
	/**
	 * V�rifier si le nom du vaccin renseig� existe dans la BDD.
	 * @param reference le nom du vaccin
	 * @return un bool�en, si c'est true il existe sinon il n'existe pas
	 */
	public boolean vaccinExiste(String reference);
	/**
	 * Modifier les information d'un vaccin
	 * @param id l'identifiant du vaccin
	 * @param reference le nom du vaccin
	 * @param laboratoire le laboratoire qui a fabriqu� le vaccin
	 * @param fabriquant le pays qui a fabriquer le vaccin
	 * @param quantite la quantit� du vaccin au stock
	 * @param depot	le d�p�t ou est stock� le vaccin
	 */
	public void modifierVaccin(int id, String reference, String laboratoire, String fabriquant, int quantite, int depot);
	/**
	 * Remplir le tableau avec tous les vaccins qui existent dans la BDD.
	 * @param table tableau d'affichage
	 */
	public void listVaccins(JTable table);
	/**
	 * Remplir le tableau avec une liste de vaccins recherch�s.
	 * @param table tableau d'affichage
	 * @param reference le nom du vaccin
	 */
	public void rechercherVaccins(JTable table, String reference);
	/**
	 * Supprimer un vaccin de la BDD.
	 * @param id l'identiffiant du vaccin
	 */
	public void retirerVaccin(int id);
	/**
	 * Retourner l'identifiant d'un vaccin.
	 * @param reference le nom du vaccin
	 * @return un entier repr�sente l'identifiant
	 */
	public int vaccinId(String reference);
	/**
	 * Retourner la r�f�rence d'un vaccin.
	 * @param id l'identifiant du vaccin
	 * @return une chaine de caract�res repr�sente la r�f�rence
	 */
	public String referenceVaccin(int id);
	/**
	 * Confirmer la commande d'un infirmier.
	 * @param id l'identifiant du vaccin
	 * @param quantite la quantit� restante
	 */
	public void livrerVaccin(int id, int quantite);
	/**
	 * Retourner la quantit� disponible d'un vaccin.
	 * @param id l'identifiant du vaccin
	 * @return un entier repr�sente la quantit�
	 */
	public int quantiteVaccin(int id);
	/**
	 * La fonction intelligente permet d'approvisionner la quantit� d'un vaccin si elle aper�oit un manque.
	 * @param id l'identifiant du vaccin
	 * @param quantite la quantit� � ajouter
	 */
	public void approvisionnementAutomatique(int id, int quantite);
	//-------------------------------------------------------------------------------------------------------------
	//Gestion des fournisseurs
	/**
	 * Ajouter un nouveau fournisseur.
	 * @param nom le nom du fournisseur
	 * @param prenom le prenom du fournisseur
	 * @param ville la ville du fournisseur
	 * @param vehicule le v�hicule du fournisseur
	 * @param telephone le num�ro de t�l�phone du fournisseur
	 */
	public void ajouterFournisseur(String nom, String prenom, String ville, String vehicule, String telephone);
	/**
	 * V�rifier si le nom du fournisseur renseig� existe dans la BDD.
	 * @param nom le nom du fournisseur
	 * @return un bool�en, si c'est true il existe sinon il n'existe pas
	 */
	public boolean fournisseurExiste(String nom);
	/**
	 * Modifier les informations d'un fournisseur.
	 * @param id l'identifiant du fournisseur
	 * @param nom le nom du fournisseur
	 * @param prenom le prenom du fournisseur
	 * @param ville la ville du fournisseur
	 * @param vehicule le v�hicule du fournisseur
	 * @param telephone le num�ro de t�l�phone du fournisseur
	 */
	public void modifierFournisseur(int id, String nom, String prenom, String ville, String vehicule, String telephone);
	/**
	 * Remplir le tableau avec tous les fournisseurs qui existent dans la BDD.
	 * @param table tableau d'affichage
	 */
	public void listFournisseurs(JTable table);
	/**
	 * Remplir le tableau avec une liste de fournisseurs recherch�s.
	 * @param table tableau d'affichage
	 * @param nom le nom du fournisseur
	 */
	public void rechercherFournisseurs(JTable table, String nom);
	/**
	 * Supprimer un fournisseur de la BDD.
	 * @param id l'identifiant du fournisseur
	 */
	public void retirerFournisseur(int id);
	//-------------------------------------------------------------------------------------------------------------
	//Gestion des commandes
	/**
	 * Cr�er une nouvelle commande.
	 * @param vaccin le vaccin � commander
	 * @param utilisateur l'utilisateur qui a command� le vaccin
	 * @param quantite la quantite de la commande
	 * @param etat l'�tat de la commande
	 */
	public void ajouterCommande(int vaccin, int utilisateur, int quantite, int etat);
	/**
	 * V�rifier si le nom du fournisseur renseig� existe dans la BDD.
	 * @param vaccin le vaccin command�
	 * @param infirmier l'utilisateur qui a command� le vaccin
	 * @return un bool�en, si c'est true elle existe sinon elle n'existe pas
	 */
	public boolean commandeExiste(int vaccin, int infirmier);
	/**
	 * Modifier les information d'une commande.
	 * @param id l'identifiant de la commande
	 * @param vaccin le vaccin � commander
	 * @param utilisateur l'utilisateur qui a command� le vaccin
	 * @param quantite la quantite de la commande
	 */
	public void modifierCommande(int id, int vaccin, int utilisateur, int quantite);
	/**
	 * Remplir le tableau avec toutes les commandes qui existent dans la BDD.
	 * @param table tableau d'affichage
	 */
	public void listCommandes(JTable table);
	/**
	 * Remplir le tableau avec une liste de toutes les commandes effectu�es par un infirmier donn�.
	 * @param table tableau d'affichage
	 * @param id l'identifiant de l'infirmier
	 */
	public void listCommandesParInfirmier(JTable table, int id);
	/**
	 * Remplir le tableau avec une liste de commandes recherch�es.
	 * @param table tableau d'affichage
	 * @param id l'identifiant de la commande
	 */
	public void rechercherCommandes(JTable table, String id);
	/**
	 * Remplir le tableau avec une liste de commandes recherch�es par un infirmier donn�.
	 * @param table tableau d'affichage
	 * @param id l'identifiant de la commande
	 * @param idInfirmier l'identifiant de l'infirmier
	 */
	public void rechercherCommandesParInfirmier(JTable table, String id, int idInfirmier);
	/**
	 * Supprimer une commande de la BDD.
	 * @param id l'identifiant de la commande
	 */
	public void annulerCommande(int id);
	/**
	 * Changer l'�tat d'une commande de "en attente" � "livr�e"
	 * @param id l'identifiant de la commande
	 */
	public void solderCommande(int id);
	/**
	 * Retourner l'�tat d'une commande effectu�e par un infirmier donn�.
	 * @param vaccin l'identifiant du vaccin
	 * @param infirmier l'identifiant de l'infirmier
	 * @return un entier repr�sente l'�tat, si c'est 0 elle est "en attente" si c'est A elle est "livr�e"
	 */
	public int etatCommande(int vaccin, int infirmier);
}

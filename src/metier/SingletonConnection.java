package metier;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Classe pour faire la connection avec la base de donn�es et instancier l'objet m�tier qui sera utiliser pour invoquer les m�thodes d�finies.
 * @author Walid MORSLI
 *
 */

public class SingletonConnection {

	private static Connection connection;
	private static DAO metier;
	/**
	 * Partie statique qui sera ex�cuter seulement une fois pour l'etablissemnet de la connection avec la BDD et l'initialisation de l'objet connection.
	 */
	static{
		try {
			metier = new DAO();
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/covid", "root", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Retourner l'objet connection
	 * @return un objet de type Connection
	 */
	public static Connection getConnection() {
		return connection;
	}
	/**
	 * Retourner l'objet metier
	 * @return un objet de type DAO
	 */
	public static DAO getMetier() {
		return metier;
	}
	
}

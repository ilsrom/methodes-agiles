package metier;

/**
 * Classe repr�sente une commande de vaccin. 
 * @author Walid MORSLI
 *
 */

public class Commande {
	
	private int id;
	private Vaccin vaccin;
	private Utilisateur utilisateur;
	private int quantite;
	private int etat;
	
	/**
	 * Construire une commande de vaccin en sp�cifiant les param�tres.
	 * @param vaccin le vaccin � commander
	 * @param utilisateur l'utilisateur qui a command� le vaccin
	 * @param quantite la quantite de la commande
	 * @param etat l'�tat de la commande
	 */
	public Commande(Vaccin vaccin, Utilisateur utilisateur, int quantite, int etat) {
		super();
		this.vaccin = vaccin;
		this.utilisateur = utilisateur;
		this.quantite = quantite;
		this.etat = etat;
	}
	/**
	 * Construire une commande de vaccin sans param�tres.
	 */
	public Commande() {
		super();
	}
	/**
	 * Retourner l'identifiant de la commande.
	 * @return un entier repr�sente l'identifiant
	 */
	public int getId() {
		return id;
	}
	/**
	 * Remplacer l'id de la commande avec un autre.
	 * @param id entier repr�sente le nouveau identifiant
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Retourner le vaccin � commander.
	 * @return le vaccin command�
	 */
	public Vaccin getVaccin() {
		return vaccin;
	}
	/**
	 * Remplacer le vaccin command� avec un autre.
	 * @param vaccin un objet Vaccin repr�sente le nouveau vaccin � commander
	 */
	public void setVaccin(Vaccin vaccin) {
		this.vaccin = vaccin;
	}
	/**
	 * Retourner l'utilisateur qui a fait la commande.
	 * @return l'utilisateur qui a fait la commande
	 */
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	/**
	 * Remplacer l'utilisateur qui a fait la commande avec un autre.
	 * @param utilisateur un objet Utilisateur repr�sente le nouveau utilisateur
	 */
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	/**
	 * Retourner la quantit� de la commande.
	 * @return la quantit� de la commande
	 */
	public int getQuantite() {
		return quantite;
	}
	/**
	 * Remplacer la quantit� de la commande avec une autre.
	 * @param quantite un entier repr�sente la nouvelle quantit�
	 */
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	/**
	 * Retourner l'�tat de la commande.
	 * @return l'�tat de la commande
	 */
	public int isEtat() {
		return etat;
	}
	/**
	 * Modifier l'�tat de la commande avec un autre.
	 * @param etat entier repr�sente le nouveau �tat
	 */
	public void setEtat(int etat) {
		this.etat = etat;
	}
	
}

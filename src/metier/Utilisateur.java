package metier;

/**
 * Classe repr�sente un utilisateur. 
 * @author Walid MORSLI
 *
 */

public class Utilisateur {
	
	private int id;
	private String role;
	private String pseudo;
	private String password;
	private String nom;
	private String prenom;
	private String telephone;
	
	/**
	 * Construire un utilisateur en sp�cifiant les param�tres.
	 * @param role le role de l'utilisateur
	 * @param pseudo le pseudo de l'utilisateur
	 * @param password le mot de passe de l'utilisateur
	 * @param nom le nom de l'utilisateur
	 * @param prenom le pr�nom de l'utilisateur
	 * @param telephone le num�ro de t�l�phone de l'utilisateur
	 */
	public Utilisateur(String role, String pseudo, String password, String nom,
			String prenom, String telephone) {
		super();
		this.role = role;
		this.pseudo = pseudo;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
	}
	/**
	 * Construire un utilisateur sans les param�tres.
	 */
	public Utilisateur() {
		super();
	}
	/**
	 * Retourner l'identifiant de l'utilisateur.
	 * @return entier repr�sente l'identifiant
	 */
	public int getId() {
		return id;
	}
	/**
	 * Remplacer l'id de l'utilisateur avec un autre.
	 * @param id un entier repr�sente le nouveau identifiant
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Retourner le role de l'utilisateur.
	 * @return chaine de caract�res repr�sente le role
	 */
	public String getRole() {
		return role;
	}
	/**
	 * Remplacer le role de l'utilisateur avec un autre.
	 * @param role une chaine de caract�res repr�sente le nouveau role
	 */
	public void setRole(String role) {
		this.role = role;
	}
	/**
	 * Retourner le pseudo de l'utilisateur.
	 * @return chaine de caract�res repr�sente le pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}
	/**
	 * Remplacer le pseudo de l'utilisateur avec un autre.
	 * @param pseudo une chaine de caract�res repr�sente le nouveau pseudo
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	/**
	 * Retourner le mot de passe de l'utilisateur.
	 * @return chaine de caract�res repr�sente le mot de passe
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * Remplacer le mot de passe de l'utilisateur avec un autre.
	 * @param password une chaine de caract�res repr�sente le nouveau mot de passe
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * Retourner le mot de nom de l'utilisateur.
	 * @return chaine de caract�res repr�sente le nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Remplacer le nom de l'utilisateur avec un autre.
	 * @param nom une chaine de caract�res repr�sente le nouveau nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Retourner le mot de pr�nom de l'utilisateur.
	 * @return chaine de caract�res repr�sente le pr�nom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * Remplacer le pr�nom de l'utilisateur avec un autre.
	 * @param prenom une chaine de caract�res repr�sente le nouveau pr�nom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * Retourner le numm�ro de t�l�phone de l'utilisateur.
	 * @return chaine de caract�res repr�sente le numm�ro de t�l�phone
	 */
	public String getTelephone() {
		return telephone;
	}
	/**
	 * Remplacer le num�ro de t�l�phone de l'utilisateur avec un autre.
	 * @param telephone une chaine de caract�res repr�sente le nouveau num�ro
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}

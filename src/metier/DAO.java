package metier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JTable;

import net.proteanit.sql.DbUtils;

public class DAO implements IDAO{

	@Override
	public Utilisateur authentifier(String pseudo, String password) {

		Connection connection = SingletonConnection.getConnection();
		Utilisateur utilisateur = new Utilisateur();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from utilisateur where pseudo = ? && password = ?");
			ps.setString(1, pseudo);
			ps.setString(2, password);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				utilisateur.setId(result.getInt("id"));
				utilisateur.setRole(result.getString("role"));
				utilisateur.setPseudo(pseudo);
				utilisateur.setPassword(password);
				utilisateur.setNom(result.getString("nom"));
				utilisateur.setPrenom(result.getString("prenom"));
				utilisateur.setTelephone(result.getString("telephone"));
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return utilisateur;
	}
	
	@Override
	public boolean infirmierExiste(String nom) {
		
		Connection connection = SingletonConnection.getConnection();
		boolean retour = false;
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from utilisateur where nom = ? and role = ?");
			ps.setString(1, nom);
			ps.setString(2, "infirmier");
			ResultSet result = ps.executeQuery();
			if(result.next()){
				retour = true;
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return retour;
	}
	
	@Override
	public int utilisateurId(String nom) {

		Connection connection = SingletonConnection.getConnection();
		int id = 0;
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from utilisateur where nom = ? and role = ?");
			ps.setString(1, nom);
			ps.setString(2, "infirmier");
			ResultSet result = ps.executeQuery();
			if(result.next()){
				id = result.getInt(1);
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return id;
		
	}
	
	@Override
	public String nomInfirmier(int id) {

		Connection connection = SingletonConnection.getConnection();
		String nom = "";
		
		try {
			PreparedStatement ps = connection.prepareStatement("select nom from utilisateur where id = ?");
			ps.setInt(1, id);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				nom = result.getString(1);
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return nom;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void ajouterVaccin(String reference, String laboratoire, String fabriquant, int quantite, int depot) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("insert into vaccin (`reference`, `laboratoire`, `fabriquant`, `quantite`, `depot`) values (?, ?, ?, ?, ?)");
			ps.setString(1, reference);
			ps.setString(2, laboratoire);
			ps.setString(3, fabriquant);
			ps.setInt(4, quantite);
			ps.setInt(5, depot);
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public void modifierVaccin(int id, String reference, String laboratoire, String fabriquant, int quantite, int depot) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("update `vaccin` set `reference` = ?, `laboratoire` = ?, `fabriquant` = ?, `quantite` = ?, `depot` = ? where id = ?");
			ps.setString(1, reference);
			ps.setString(2, laboratoire);
			ps.setString(3, fabriquant);
			ps.setInt(4, quantite);
			ps.setInt(5, depot);
			ps.setInt(6, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		
	}

	@Override
	public void listVaccins(JTable table) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from vaccin");
			ResultSet result = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(result));
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void retirerVaccin(int id) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("delete from `vaccin` where id = ?");
			ps.setInt(1, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		
	}

	@Override
	public void rechercherVaccins(JTable table, String reference) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from vaccin where reference like ?");
			ps.setString(1, "%"+reference+"%");
			ResultSet result = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(result));
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean vaccinExiste(String reference) {
		
		Connection connection = SingletonConnection.getConnection();
		boolean retour = false;
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from vaccin where reference = ?");
			ps.setString(1, reference);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				retour = true;
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return retour;
	}
	
	@Override
	public int vaccinId(String reference) {

		Connection connection = SingletonConnection.getConnection();
		int id = 0;
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from vaccin where reference = ?");
			ps.setString(1, reference);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				id = result.getInt(1);
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return id;
		
	}
	
	@Override
	public String referenceVaccin(int id) {

		Connection connection = SingletonConnection.getConnection();
		String reference = "";
		
		try {
			PreparedStatement ps = connection.prepareStatement("select reference from vaccin where id = ?");
			ps.setInt(1, id);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				reference = result.getString(1);
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return reference;
		
	}
	
	@Override
	public void livrerVaccin(int id, int quantite) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("update `vaccin` set `quantite` = ? where id = ?");
			ps.setInt(1, quantite);
			ps.setInt(2, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public int quantiteVaccin(int id) {

		Connection connection = SingletonConnection.getConnection();
		int quantite = 0;
		
		try {
			PreparedStatement ps = connection.prepareStatement("select quantite from vaccin where id = ?");
			ps.setInt(1, id);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				quantite = result.getInt(1);
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return quantite;
		
	}
	
	@Override
	public void approvisionnementAutomatique(int id, int quantite) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("update `vaccin` set `quantite` = ? where id = ?");
			ps.setInt(1, quantite);
			ps.setInt(2, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void ajouterFournisseur(String nom, String prenom, String ville,
			String vehicule, String telephone) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("insert into fournisseur (`nom`, `prenom`, `ville`, `vehicule`, `telephone`) values (?, ?, ?, ?, ?)");
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setString(3, ville);
			ps.setString(4, vehicule);
			ps.setString(5, telephone);
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean fournisseurExiste(String nom) {
		
		Connection connection = SingletonConnection.getConnection();
		boolean retour = false;
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from fournisseur where nom = ?");
			ps.setString(1, nom);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				retour = true;
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return retour;
		
	}

	@Override
	public void modifierFournisseur(int id, String nom, String prenom,
			String ville, String vehicule, String telephone) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("update `fournisseur` set `nom` = ?, `prenom` = ?, `ville` = ?, `vehicule` = ?, `telephone` = ? where id = ?");
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setString(3, ville);
			ps.setString(4, vehicule);
			ps.setString(5, telephone);
			ps.setInt(6, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void listFournisseurs(JTable table) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from fournisseur");
			ResultSet result = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(result));
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void rechercherFournisseurs(JTable table, String nom) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from fournisseur where nom like ?");
			ps.setString(1, "%"+nom+"%");
			ResultSet result = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(result));
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void retirerFournisseur(int id) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("delete from `fournisseur` where id = ?");
			ps.setInt(1, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void ajouterCommande(int vaccin, int utilisateur, int quantite,
			int etat) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("insert into commande (`id_vaccin`, `id_utilisateur`, `quantite`, `etat`) values (?, ?, ?, ?)");
			ps.setInt(1, vaccin);
			ps.setInt(2, utilisateur);
			ps.setInt(3, quantite);
			ps.setInt(4, etat);
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean commandeExiste(int vaccin, int infirmier) {

		Connection connection = SingletonConnection.getConnection();
		boolean retour = false;
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from commande where id_vaccin = ? and id_utilisateur = ?");
			ps.setInt(1, vaccin);
			ps.setInt(2, infirmier);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				retour = true;
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return retour;
		
	}

	@Override
	public void modifierCommande(int id, int vaccin, int utilisateur,
			int quantite) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("update `commande` set `id_vaccin` = ?, `id_utilisateur` = ?, `quantite` = ? where id = ?");
			ps.setInt(1, vaccin);
			ps.setInt(2, utilisateur);
			ps.setInt(3, quantite);
			ps.setInt(4, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void listCommandes(JTable table) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from commande");
			ResultSet result = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(result));
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void rechercherCommandes(JTable table, String id) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from commande where id like ?");
			ps.setString(1, "%"+id+"%");
			ResultSet result = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(result));
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void annulerCommande(int id) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("delete from `commande` where id = ?");
			ps.setInt(1, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void solderCommande(int id) {
		
		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("update `commande` set `etat` = ? where id = ?");
			ps.setInt(1, 1);
			ps.setInt(2, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public int etatCommande(int vaccin, int infirmier) {

		Connection connection = SingletonConnection.getConnection();
		int etat = -1;
		
		try {
			PreparedStatement ps = connection.prepareStatement("select etat from commande where id_vaccin = ? and id_utilisateur = ?");
			ps.setInt(1, vaccin);
			ps.setInt(2, infirmier);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				if(result.getInt(1) == 0){
					etat = 0;
				}
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return etat;
		
	}

	@Override
	public void listCommandesParInfirmier(JTable table, int id) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from commande where id_utilisateur = ?");
			ps.setInt(1, id);
			ResultSet result = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(result));
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void rechercherCommandesParInfirmier(JTable table, String id,
			int idInfirmier) {

		Connection connection = SingletonConnection.getConnection();
		
		try {
			PreparedStatement ps = connection.prepareStatement("select * from commande where id like ? and id_utilisateur = ?");
			ps.setString(1, "%"+id+"%");
			ps.setInt(2, idInfirmier);
			ResultSet result = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(result));
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}

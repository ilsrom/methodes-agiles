package metier;

/**
 * Classe repr�sente un fournisseur. 
 * @author Walid MORSLI
 *
 */

public class Fournisseur {

	private int id;
	private String nom;
	private String prenom;
	private String ville;
	private String vehicule;
	private String telephone;
	
	/**
	 * Construire un fournisseur de vaccin en sp�cifiant les param�tres.
	 * @param nom le nom du fournisseur
	 * @param prenom le prenom du fournisseur
	 * @param ville la ville du fournisseur
	 * @param vehicule le v�hicule du fournisseur
	 * @param telephone le num�ro de t�l�phone du fournisseur
	 */
	public Fournisseur(String nom, String prenom, String ville,
			String vehicule, String telephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.ville = ville;
		this.vehicule = vehicule;
		this.telephone = telephone;
	}
	/**
	 * Construire un fournisseur de vaccin sans param�tres.
	 */
	public Fournisseur() {
		super();
	}
	/**
	 * Retourner l'identifiant du fournisseur.
	 * @return entier repr�sente l'identifiant
	 */
	public int getId() {
		return id;
	}
	/**
	 * Remplacer l'id du fournisseur avec un autre.
	 * @param id un entier repr�sente le nouveau identifiant
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Retourner le nom du fournisseur.
	 * @return chaine de caract�res repr�sente le nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Remplacer le nom du fournisseur avec un autre.
	 * @param nom une chaine de caract�res repr�sente le nouveau nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Retourner le pr�nom du fournisseur.
	 * @return chaine de caract�res repr�sente le pr�nom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * Remplacer le pr�nom du fournisseur avec un autre.
	 * @param prenom une chaine de caract�res repr�sente le nouveau pr�nom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * Retourner la ville ou habite le fournisseur.
	 * @return chaine de caract�res repr�sente la ville
	 */
	public String getVille() {
		return ville;
	}
	/**
	 * Remplacer la ville du fournisseur avec une autre.
	 * @param ville une chaine de caract�res repr�sente la nouvelle ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	/**
	 * Retourner le nom de v�hicule du fournisseur.
	 * @return chaine de caract�res repr�sente le nom de v�hicule
	 */
	public String getVehicule() {
		return vehicule;
	}
	/**
	 * Remplacer le nom de v�hicule du fournisseur avec un autre.
	 * @param vehicule une chaine de caract�res repr�sente le nouveau type de v�hicule
	 */
	public void setVehicule(String vehicule) {
		this.vehicule = vehicule;
	}
	/**
	 * Retourner le num�ro de t�l�phone du fournisseur.
	 * @return chaine de caract�res repr�sente le num�ro de t�l�phone
	 */
	public String getTelephone() {
		return telephone;
	}
	/**
	 * Remplacer le num�ro de t�l�phone du fournisseur avec un autre.
	 * @param telephone une chaine de caract�res repr�sente le nouveau num�ro
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
}
